interface IMessage
{
    name: string;
    id?: number;
    payload?: any;
    error?: IError;
    via: "MessageTunnel";
}

interface IListenerCallback
{
    (payload?: any, respond?: IPingCallback): any;
}

interface IPingCallback
{
    (err?: Error, payload?: any): any;
}

interface IError
{
    name: string;
    message: string;
}

namespace MessageTunnel
{
    var _portals: Portal[] = [];
    
    function _sendError (err: Error): IError
    {
        if (!err)
            return
        return { name: err.name, message: err.message };
    }

    function _receiveError (error: IError): Error
    {
        if (!error)
            return;
        let err = new Error(error.message);
        err.name = error.name;
        return err;
    }
    
    export class Portal
    {
        private _listeners: { [index: string]: IListenerCallback } = {};
        private _pings: { [index: number]: IPingCallback } = {};
        private _nextPingId: number = 0;
        
        private _target: Window;
        
        constructor (target: Window)
        {
            this._target = target;
        }
        
        getTarget (): Window
        {
            return this._target;
        }
        
        setListener (name: string, callback: IListenerCallback)
        {
            // listen for message
            
            if (!name || !callback)
                console.log("Warning: Cannot add listener.");
            else
                this._listeners[name] = callback;
        }
        
        removeListener (name: string)
        {
            // stop listening
            
            delete this._listeners[name];
        }
        
        sendMessage (name: string, payload?: any, callback?: IPingCallback)
        {
            // send message
            
            if (!name || name == "_pong")
                throw new Error("Name is invalid.");
            
            let message: IMessage = {
                name: name,
                id: this._addPing(callback),
                payload: payload,
                via: "MessageTunnel"
            };
            this._target.postMessage(message, "*");
        }
        
        messageReceived (data: IMessage, origin: string)
        {
            // message received
            
            let callback = this._listeners[data.name];
            if (!callback)
                return;
            
            callback(data.payload, (err?: Error, payload?: any) => {
                let message: IMessage = {
                    name: "_pong",
                    id: data.id,
                    error: _sendError(err),
                    payload: payload,
                    via: "MessageTunnel"
                };
                this._target.postMessage(message, origin);
            });
        }
        
        pongReceived (data: IMessage)
        {
            // pong received

            let callback = this._pings[data.id];
            if (callback)
                callback(_receiveError(data.error), data.payload);
            
            delete this._pings[data.id];
        }
        
        private _pingTimeout (id: number)
        {
            // no response in time
            
            let callback = this._pings[id];
            if (callback) {
                let err = new Error("Message timed out.");
                err.name = "TimeoutError";
                callback(err);
            }
            
            delete this._pings[id];
        }
        
        private _addPing (callback?: IPingCallback): number
        {
            // expect a pong response
            
            if (!callback)
                return;
            
            let id = this._nextPingId;
            this._nextPingId++;
            this._pings[id] = callback;
            
            setTimeout(() => { this._pingTimeout(id); }, 4000);
            return id;
        }
    }

    export function getParentPortal (): Portal
    {
        return ((window.self !== window.top) ? getPortal(window.parent) : undefined);
    }
    
    export function getPortal (target: Window): Portal
    {
        // exists?
        
        for (let portal of _portals) {
            if (portal.getTarget() === target)
                return portal;
        }
        
        // create
        
        let portal = new Portal(target);
        _portals.push(portal);
        return portal;
    }
    
    export function setListener (source: Window, name: string, callback: IListenerCallback)
    {
        getPortal(source).setListener(name, callback);
    }
    
    export function removeListener (source: Window, name: string)
    {
        getPortal(source).removeListener(name);
    }
    
    export function sendMessage (target: Window, name: string, payload?: any, callback?: IPingCallback)
    {
        getPortal(target).sendMessage(name, payload, callback);
    }
    
    function _onMessage (e: MessageEvent)
    {
        // message event received
        
        if (!(e.data instanceof Object) || e.data['via'] !== "MessageTunnel")
            return;
        
        if (e.data['name'] == "_pong")
            getPortal(e.source).pongReceived(e.data);
        else
            getPortal(e.source).messageReceived(e.data, e.origin);
    }
    
    window.addEventListener('message', _onMessage);
}
