var MessageTunnel;
(function (MessageTunnel) {
    var _portals = [];
    function _sendError(err) {
        if (!err)
            return;
        return { name: err.name, message: err.message };
    }
    function _receiveError(error) {
        if (!error)
            return;
        var err = new Error(error.message);
        err.name = error.name;
        return err;
    }
    var Portal = (function () {
        function Portal(target) {
            this._listeners = {};
            this._pings = {};
            this._nextPingId = 0;
            this._target = target;
        }
        Portal.prototype.getTarget = function () {
            return this._target;
        };
        Portal.prototype.setListener = function (name, callback) {
            // listen for message
            if (!name || !callback)
                console.log("Warning: Cannot add listener.");
            else
                this._listeners[name] = callback;
        };
        Portal.prototype.removeListener = function (name) {
            // stop listening
            delete this._listeners[name];
        };
        Portal.prototype.sendMessage = function (name, payload, callback) {
            // send message
            if (!name || name == "_pong")
                throw new Error("Name is invalid.");
            var message = {
                name: name,
                id: this._addPing(callback),
                payload: payload,
                via: "MessageTunnel"
            };
            this._target.postMessage(message, "*");
        };
        Portal.prototype.messageReceived = function (data, origin) {
            // message received
            var _this = this;
            var callback = this._listeners[data.name];
            if (!callback)
                return;
            callback(data.payload, function (err, payload) {
                var message = {
                    name: "_pong",
                    id: data.id,
                    error: _sendError(err),
                    payload: payload,
                    via: "MessageTunnel"
                };
                _this._target.postMessage(message, origin);
            });
        };
        Portal.prototype.pongReceived = function (data) {
            // pong received
            var callback = this._pings[data.id];
            if (callback)
                callback(_receiveError(data.error), data.payload);
            delete this._pings[data.id];
        };
        Portal.prototype._pingTimeout = function (id) {
            // no response in time
            var callback = this._pings[id];
            if (callback) {
                var err = new Error("Message timed out.");
                err.name = "TimeoutError";
                callback(err);
            }
            delete this._pings[id];
        };
        Portal.prototype._addPing = function (callback) {
            // expect a pong response
            var _this = this;
            if (!callback)
                return;
            var id = this._nextPingId;
            this._nextPingId++;
            this._pings[id] = callback;
            setTimeout(function () { _this._pingTimeout(id); }, 4000);
            return id;
        };
        return Portal;
    }());
    MessageTunnel.Portal = Portal;
    function getParentPortal() {
        return ((window.self !== window.top) ? getPortal(window.parent) : undefined);
    }
    MessageTunnel.getParentPortal = getParentPortal;
    function getPortal(target) {
        // exists?
        for (var _i = 0, _portals_1 = _portals; _i < _portals_1.length; _i++) {
            var portal_1 = _portals_1[_i];
            if (portal_1.getTarget() === target)
                return portal_1;
        }
        // create
        var portal = new Portal(target);
        _portals.push(portal);
        return portal;
    }
    MessageTunnel.getPortal = getPortal;
    function setListener(source, name, callback) {
        getPortal(source).setListener(name, callback);
    }
    MessageTunnel.setListener = setListener;
    function removeListener(source, name) {
        getPortal(source).removeListener(name);
    }
    MessageTunnel.removeListener = removeListener;
    function sendMessage(target, name, payload, callback) {
        getPortal(target).sendMessage(name, payload, callback);
    }
    MessageTunnel.sendMessage = sendMessage;
    function _onMessage(e) {
        // message event received
        if (!(e.data instanceof Object) || e.data['via'] !== "MessageTunnel")
            return;
        if (e.data['name'] == "_pong")
            getPortal(e.source).pongReceived(e.data);
        else
            getPortal(e.source).messageReceived(e.data, e.origin);
    }
    window.addEventListener('message', _onMessage);
})(MessageTunnel || (MessageTunnel = {}));
